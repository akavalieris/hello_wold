from lxml import html
import requests
import re

hwpage = requests.get('https://en.wikipedia.org/wiki/%22Hello,_World!%22_program')
tree = html.fromstring(hwpage.content)
title = tree.xpath('//*[@id="firstHeading"]/text()')[0]

pattern = r'"(.*?)"'
m = re.match(pattern, title)

hw = m.group()
hw = hw.replace('"', '')

letters = list(hw)
progress = 0.0

def get_letter_page(letter):
    page = requests.get('https://en.wikipedia.org/wiki/' + letter)
    tree = html.fromstring(page.content)
    returned_letter = tree.xpath('//*[@id="firstHeading"]/text()')[0]

    if returned_letter == 'Comma':
        returned_letter = ','
    elif returned_letter == 'Bad title':
        returned_letter = ' '
    elif returned_letter == 'Exclamation mark':
        returned_letter = '!'
    elif letter.islower():
        returned_letter = returned_letter.lower()

    return returned_letter

#print("assembling result...")
output = ""
for l in letters:
    output += get_letter_page(l)
    progress += 1.0
    #print("progress: " + "{:3.1f}".format(100 * progress / len(letters)) + "%")

print(output)
